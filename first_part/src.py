from math import prod

def exercise_one():
    """fizzbuzz alternative implementation"""
    for i in range(1, 101):
        # divisibility by 15 also valid
        if (i % 3) == 0 and (i % 5) == 0:
            print("ThreeFive")
        elif i % 3 == 0:
            print("Three")
        elif i % 5 == 0:
            print("Five")
        else:
            print(i)

def exercise_two():
    """Exercise 2 driver"""
    print("263 -->", is_colorful(263))
    print("236 -->", is_colorful(236))
    print("2532 -->", is_colorful(2532))

def exercise_three():
    """Exercise 3 driver"""
    print(calculate(['4', '3', '-2']))
    print(calculate(453))
    print(calculate(['nothing', 3, '8', 2, '1']))
    print(calculate('54'))

def exercise_four():
    """Exercise 4 driver"""
    print(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']))
    print(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']))
    print(anagrams('laser', ['lazing', 'lazy',  'lacer']))

def is_colorful(n):
    """Test if a function is colorful return True if so and False otherwise"""
    # if number between 0 and 10 the number is trivially colorful
    if 0 <= n < 10:
        return True

    # collect the digits of n
    dig = [int(c) for c in str(n)] 

    # if the number contains the digits 0, 1 or doesn't contain unique digits
    # the number is not colorful 
    if 1 in dig or 0 in dig or len(dig) > len(set(dig)):
        return False
    
    # products contains the digits of n
    products = list(set(dig))

    # loop through and add the products to the list
    for i in range(len(dig)):
        for j in range(i+2, len(dig)+1):
            p = prod(dig[i:j])
            if p in products:
                return False
            products.append(p)
    return True

def calculate(lst):
    """Return sum of string numbers"""
    if isinstance(lst, list):
        total = 0
        for i in lst:
            if isinstance(i, str) and i.lstrip('+-').isdigit():
                total += int(i)
        return total
    else:
        return False

def anagrams(word, grams):
    """Return a list of anagrams of a word from a list of possible anagrams"""
    result = [gr for gr in grams if sorted(word) == sorted(gr)]
    return result