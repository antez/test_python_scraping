import scrapy
from scrapy_playwright.page import PageMethod
from spidy.items import ProductItem


class ProductsSpider(scrapy.Spider):
    name = "products"
    def start_requests(self):
        url = "http://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas" 
        yield scrapy.Request(url, 
        meta=dict(
            playwright = True,
            playwright_include_page = True,
            playwright_page_methods = [
                PageMethod('wait_for_selector', 'shared-product-tile')
            ]
        ), 
        errback=self.errback)

    def parse(self, response):
        product_item = ProductItem()

        for product in response.css('shared-product-tile'):
            product_item['price'] = product.css('div.product-tile-price div.primary::text').extract()
            product_item['title'] = product.css('a.product-title-link.ng-star-inserted::text').extract()
            yield product_item
    
    async def errback(self, failure):
        page = failure.request_meta["playwright_page"]
        await page.close()