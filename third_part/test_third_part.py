import requests
from src import http_request, Product
import os

def test_http_request():
    res1 = http_request({}, {'isadmin': 1})
    res2 = http_request({'user-agent': 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US);'}, '')
    assert 200 == res1.status_code
    assert 200 == res2.status_code

def test_product_class():
    products = Product()
    products.save()
    assert True == isinstance(products, Product)
    assert len(products.data) >= 0
    assert products.is_available('not there') == False
    assert products.is_available('Tommee Tippee Ctn Transition Cup') == True
    assert os.path.isfile('products.csv') == True