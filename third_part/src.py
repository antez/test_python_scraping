import requests
import gzip
import os
import json
import csv

def http_request(header, payload):
    """send http request with given header and payload"""
    resp = requests.post('https://httpbin.org/anything', data = payload, headers = header)
    return resp

class Product():
    def __init__(self):
        # import the data from file
        path = os.path.dirname(__file__) + '/data/data.json.gz'
        with gzip.open(path, 'rb') as f:
            self.data = json.loads(f.read())
        
        # sanitize data
        first_product = self.data['Bundles'][0]['Product'][0]
        temp = [{ 'Name': first_product['Name'], 'Price': first_product['Price'], 'IsAvailable': first_product['IsAvailable']}]
        for product in self.data['Bundles']:
            try:
                temp.append({'Name': product['Products'][0]['Name'], 'Price': product['Products'][0]['Price'], 'IsAvailable': product['Products'][0]['IsAvailable']})
            except (KeyError, IndexError):
                continue

        self.data = temp

    def is_available(self, product_name):
        """check product given by product name's availability"""
        for product in self.data:
            if product['Name'] == product_name:
                return bool(product['IsAvailable'])
        return False

    def print_products(self):

        for product in self.data:
            # truncate name
            name = product['Name'][:30]
            # convert to float and dd.d format
            try:
                price = '%.1f' % float(product['Price'])
            except:
                price = None
            print(f"You can buy {name} at our store at {price}")

    def save(self):
        """save file as csv"""
        with open('products.csv', 'w') as f:
            writer = csv.DictWriter(f, fieldnames = ["Name", "Price", "IsAvailable"])
            writer.writeheader()
            writer.writerows(self.data)