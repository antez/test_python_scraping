import random

def random_gen():
    """generate random numbers between 10 and 20, yields until 15 is encountered"""
    while True:
        n = random.randrange(10, 20)
        if n != 15:
            yield n
        else:
            yield n
            break

def decorator_to_str(func):
    """stringify output of function"""
    def wrapper(*args, **kwargs):
        return str(func(*args, **kwargs))
    return wrapper

@decorator_to_str
def add(a, b):
    return a + b

@decorator_to_str
def get_info(d):
    return d['info']

def ignore_exception(exception):
    """ignore specific exception inside a function"""
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                return None
        return wrapper
    return decorator

@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b

@ignore_exception(TypeError)
def raise_something(exception):
    raise exception

# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap

@CacheDecorator()
def fib(n):
    """Fibonacci function returns the sum of the previous two natural numbers"""
    if n < 2:
        return n
    else:
        return fib(n-1) + fib(n - 2)

class MetaInherList(type):
    def __new__(self, name, bases, attrs):
        kls = type(name, bases, attrs)
        return kls

class Ex:
    x = 4

class ForceToList(Ex, metaclass=MetaInherList):
    data = []

    def __init__(self, data):
        self.data = data
    
    def __getitem__(self, index):
        return self.data[index]
    
    def __len__(self):
        return len(self.data)

class ProcessChecker(type):
    """Process method tester"""
    def __new__(self, name, bases, attrs):
        kls = type(name, bases, attrs)
        if hasattr(kls, 'process') and callable(getattr(kls, 'process')):
            return kls
        else:
            return None

class WithProcess(metaclass=ProcessChecker):
    """Example class with process method"""
    def process():
        pass


class WithoutProcess(metaclass=ProcessChecker):
    """Example class without process method"""
    def __init__(self):
        pass